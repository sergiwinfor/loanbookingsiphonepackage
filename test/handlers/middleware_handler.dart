part of 'handlers.dart';

void middlewareHandlerTests() {
  group("MiddlewareHandlerGroup", () {
    test(
        "Creación de una cadena de  middlewares a partir de una lista de middlewares",
        () {
      final first = PassMiddlewareHandler();
      final second = PassMiddlewareHandler();
      final third = PassMiddlewareHandler();
      final middlewares = [first, second, third];

      final pipe = MiddlewareHandler.buildPipe(middlewares);

      assert(pipe == first);
      assert(pipe.next == second);
      assert(pipe.next.next == third);
    });
  });

  test(
      "Intentar crear una cadena de middlewares  a partir de una lista vacía provoca una BuildingPipeException",
      () {
    expect(() => MiddlewareHandler.buildPipe([]),
        throwsA(TypeMatcher<BuildingPipeException>()));
  });

  test(
      "Un DelegateMiddleware delega en su handler subyacente en caso de que este pueda procesar el evento ",
      () async {
    final innerHandler = EventAHandler();
    final delegateHandler = DelegateMiddleware(innerHandler);
    var response = await delegateHandler.handle(EventA(), Store());
    assert(response is EventAResponse);
  });
  test(
      "Un DelegateMiddleware llama al siguiente middleware en caso de que su handler subyacente no pueda procesar el evento ",
      () async {
    final innerHandler = CustomEventHandler<EventA, EventAResponse>(
        expectAsync3((e, s, sink) => EventAResponse()));
    final delegateHandler = DelegateMiddleware(innerHandler);
    delegateHandler.handle(EventAB(), Store());
  });
  test(
      "Una llamada a callnext siendo next == null provoca un excepción de tipo UnexpectedEndOfPipeException",
      () {
    final middleware = PassMiddlewareHandler();
    expect(() => middleware.callNext(EventAB(), Store(), null),
        throwsA(TypeMatcher<UnexpectedEndOfPipeException>()));
  });
  test(
      "Una llamada a callnext siendo next != null llama efectivamente a next.handle",
      () {
    final next = DelegateMiddleware(CustomEventHandler<EventA, EventAResponse>(
        expectAsync3((e, s, sink) => EventAResponse())));
    final first = PassMiddlewareHandler();

    first.next = next;
    first.callNext(EventAB(), Store(), null);
  });

  
}
