import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:gabito/gabito.dart';
import 'package:gabito/src/handlers/handler.dart';
import 'package:gabito/src/view_model.dart';
import 'package:matcher/matcher.dart';
import 'handlers/handlers.dart';

void viewModelTests() {
  group("ViewModel", () {
    // test(
    //     "Un viewmodel lanza una excepción de tipo NoSuitabledHandlerForEvent en caso de que le envien un evento que su handler no pueda manejar",
    //     () {
    //   final handler = EventAHandler();
    //   final vm = TestViewModel(handler: handler, store: Store());
    //   expect(() => vm.initEventsProcessing(),
    //       throwsA(TypeMatcher<NoSuitabledHandlerForEvent>()));
    //   vm.newEvent(EventA());
    // });
    test("Un viewmodel delega el procesamiento de un evento a su handler", () {
      final handler = CustomEventHandler<EventA, EventAResponse>(
          expectAsync3((e, s, sink) => EventAResponse()));
      final vm = TestViewModel(handler: handler, store: Store());
      vm.newEvent(EventA());
    });

    test(
        "Cada envío al eventSink realizado por el handler del vm es equivalente al envio de un evento desde el propio vm",
        () {
      final first = CustomMiddlewareHandler(expectAsync4((e, s, sink, next) {
        if (e is EventAA) {
          sink.add(EventAB());
          sink.add(EventA());
        }
        return next.handle(e, s, eventSink: sink);
      }, count: 3));

      final second = DelegateMiddleware(
          CustomEventHandler<EventAB, EventABResponse>(
              expectAsync3((e, s, sink) => EventABResponse())));
      final last = DelegateMiddleware(
          CustomEventHandler<EventA, EventAResponse>(expectAsync3((e, s, sink) {
        print("last");
        return EventAResponse();
      }, count: 2)));
      final middlewares = [first, second, last];

      final pipe = MiddlewareHandler.buildPipe(middlewares);

      final vm = TestViewModel(handler: pipe, store: Store());
      vm.newEvent(EventAA());
    });
    test(
        "La respuesta del handler se emite por el stream de respuestas como un EventProcessingResult",
        () {
      final testResponse = EventAResponse();
      final handler = CustomEventHandler<EventA, EventAResponse>((e, s, sink) {
        return testResponse;
      });

      final vm = TestViewModel(handler: handler, store: Store());

      final onResponse = expectAsync1(
        (EventProcessingResult resp) {
          expect(resp.response, testResponse);
        },
      );
      vm.responses.listen(onResponse);

      vm.newEvent(EventA());
    });

    test(
        "El error lanzado por un handler se emite por el stream de respuestas como un error de tipo EventProcessingResult",
        () {
      final testException = Exception();
      final handler = CustomEventHandler<EventA, EventAResponse>((e, s, sink) {
        throw testException;
      });

      final vm = TestViewModel(handler: handler, store: Store());

      final onError = expectAsync1(
        (EventProcessingResult err) {
          expect(err.response, testException);
        },
      );
      vm.responses.listen((_) {}, onError: onError);

      vm.newEvent(EventA());
    });

    test(
        "El stream devuelto por newEvent devuelve la respuesta al evento añadido en caso de exito",
        () {
      final testResponse = EventAResponse();
      final handler = CustomEventHandler<EventA, EventAResponse>((e, s, sink) {
        return testResponse;
      });

      final vm = TestViewModel(handler: handler, store: Store());

      final onResponse = expectAsync1(
        (resp) {
          expect(resp, testResponse);
        },
      );

      vm.newEvent(EventA()).listen(onResponse);
    });

    test(
        "El stream devuelto por newEvent devuelve el error lanzado por el handler en respuesta al evento añadido en caso de fallo",
        () {
      final testException = Exception();
      final handler = CustomEventHandler<EventA, EventAResponse>((e, s, sink) {
        throw testException;
      });

      final vm = TestViewModel(handler: handler, store: Store());

      final onError = expectAsync1(
        (err) {
          expect(err, testException);
        },
      );

      vm.newEvent(EventA()).listen((_) {}, onError: onError);
    });
  });
}

class TestViewModel extends ViewModel with HandlerResponsesUtilsMixin {
  TestViewModel({EventHandler handler, Store store})
      : super(handler: handler, store: store);
}
