export 'final/router_event_handler.dart';
export 'middleware/middleware.dart';
import 'dart:async';
import 'package:gabito/src/store.dart';

abstract class EventHandler<E, R> {
  bool canHandle(e) => e is E;

  FutureOr<R> handle(E event, Store store, {StreamSink eventSink});
}

abstract class FinalHandler<E, R> extends EventHandler<E, R> {}
